<?php

namespace Drupal\vimeo_video_uploader\Form;

use Drupal\Core\Entity\EntityManager;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;

/**
 * Provide configuration form for user to provide vimeo API information for a.
 */
class VimeoSettingsForm extends ConfigFormBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;
  /**
   * The entity manager.
   *
   * @var \Drupal\Core\Entity\EntityManager
   */
  protected $entityManager;

  /**
   * Construct function.
   */
  public function __construct(ConfigFactoryInterface $config_factory, LoggerChannelFactoryInterface $logger_factory, EntityTypeManagerInterface $entity_type_manager, EntityManager $entity_manager) {
    $this->setConfigFactory($config_factory);
    $this->logger = $logger_factory->get('vimeo_video_uploader');
    $this->entityTypeManager = $entity_type_manager;
    $this->entityManager = $entity_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
        $container->get('config.factory'),
        $container->get('logger.factory'),
        $container->get('entity_type.manager'),
        $container->get('entity.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'vimeo_video_uploader_configuration_form';
  }

  /**
   * {@inheritdoc}
   */
  public function getEditableConfigNames() {
    return ['vimeo_video_uploader.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    // Get the configuration from ConfigFormBase::config().
    $config = self::config('vimeo_video_uploader.settings');
    $form['values'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('VIMEO VIDEO UPLOAD CONFIGURATION'),
      '#collapsible' => TRUE,
      '#tree' => TRUE,
    ];

    $form['values']['client_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Enter (Vimeo Client Identifier)'),
      '#default_value' => $config->get('values.client_id'),
      '#required' => TRUE,
    ];

    $form['values']['client_secret'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Enter (Vimeo Client Secrets)'),
      '#default_value' => $config->get('values.client_secret'),
      '#required' => TRUE,
    ];

    $form['values']['access_token'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Enter generated (Your new Access token)'),
      '#default_value' => $config->get('values.access_token'),
      '#required' => TRUE,
    ];
    $form['values']['content_type_select'] = [
      '#type' => 'select',
      '#title' => $this->t('Select the Content Types from which you have to upload video to Vimeo'),
      '#options' => $this->getContentTypeList(),
      '#default_value' => $config->get('values.content_type_select'),
      '#required' => TRUE,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * Get list of all content type by this function.
   */
  public function getContentTypeList() {
    $contentTypes = $this->entityTypeManager->getStorage('node_type')->loadMultiple();
    $contentTypesList = [];

    foreach ($contentTypes as $contentType) {
      $contentTypesList[$contentType->id()] = $contentType->label();
    }

    return $contentTypesList;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Set configuration.
    $config = self::config('vimeo_video_uploader.settings');
    $form_state_values = $form_state->getValues();
    $message = "Saved the Vimeo configuration.";
    $old_content_type_select = $config->get('values.content_type_select');
    if ($old_content_type_select !== $form_state_values['values']['content_type_select']) {
      // Delete some things.
      $fields = $this->entityManager->getFieldDefinitions('node', $old_content_type_select);

      if (isset($fields['field_vimeo_file_browse']) && ($field = $fields['field_vimeo_file_browse'])) {
        $field->delete();
      }
      if (isset($fields['field_embeddedvideo']) && $field1 = $fields['field_embeddedvideo']) {
        $field1->delete();
      }
      $this->addContentTypeField($form_state_values['values']['content_type_select']);
      $message = "Created 'Browse video for uploading to Vimeo' field in '" . strtoupper($form_state_values['values']['content_type_select']) . "' Content type.";
    }
    $config
      ->set('values.client_id', $form_state_values['values']['client_id'])
      ->set('values.client_secret', $form_state_values['values']['client_secret'])
      ->set('values.access_token', $form_state_values['values']['access_token'])
      ->set('values.content_type_select', $form_state_values['values']['content_type_select']);

    $config->save();
    drupal_set_message($message, 'status');
  }

  /**
   * Add form fields in selected content type.
   */
  public function addContentTypeField($bundle) {

    FieldStorageConfig::create([
      'field_name' => 'field_vimeo_file_browse',
      'entity_type' => 'node',
      'type' => 'file',
        // 'cardinality' => -1,.
    ])->save();
    FieldConfig::create([
      'field_name' => 'field_vimeo_file_browse',
      'entity_type' => 'node',
      'bundle' => $bundle,
      'settings' => ['file_extensions' => 'mp4 mov wmv avi flv'],
      'label' => 'Browse video for uploading to Vimeo',
    ])->save();

    entity_get_form_display('node', $bundle, 'default')
      ->setComponent('field_vimeo_file_browse', [
        'type' => 'file_generic',
      ])
      ->save();

    entity_get_display('node', $bundle, 'default')
      ->setComponent('field_vimeo_file_browse', [
        'type' => 'file_default',
      ])
      ->save();

    // Add embedded video input field.
    FieldStorageConfig::create([
      'field_name' => 'field_embeddedvideo',
      'entity_type' => 'node',
      'type' => 'video_embed_field',
        // 'cardinality' => -1,.
    ])->save();

    FieldConfig::create([
      'field_name' => 'field_embeddedvideo',
      'entity_type' => 'node',
      'bundle' => $bundle,
      'label' => 'Vimeo video link',
        // 'settings' => array('allowed_providers' => ['vimeo','youtube']),.
    ])->save();

    entity_get_form_display('node', $bundle, 'default')
      ->setComponent('field_embeddedvideo', [
        'type' => 'video_embed_field_textfield',
        'class' => 'neera',
      ])
      ->save();

    entity_get_display('node', $bundle, 'default')
      ->setComponent('field_embeddedvideo', [
        'type' => 'video_embed_field_video',
      ])
      ->save();

  }

}
